package org.gkaltner.akkodisjavatest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AkkodisJavaTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(AkkodisJavaTestApplication.class, args);
    }

}
