package org.gkaltner.akkodisjavatest.adapters.controllers;

import org.gkaltner.akkodisjavatest.infraestructure.dtos.PriceResponseDto;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

public interface IPriceControllerAdapter {
    Mono<PriceResponseDto> getPVP(int brandId, long product, LocalDateTime dateTime);
}
