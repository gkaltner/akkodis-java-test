package org.gkaltner.akkodisjavatest.adapters.controllers;

import lombok.extern.slf4j.Slf4j;
import org.gkaltner.akkodisjavatest.domain.services.IGetPriceService;
import org.gkaltner.akkodisjavatest.infraestructure.dtos.PriceResponseDto;
import org.gkaltner.akkodisjavatest.infraestructure.mappers.PriceMapper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Slf4j
@Service
public class PriceControllerAdapter implements IPriceControllerAdapter {
    private final IGetPriceService getPriceService;
    private final PriceMapper priceMapper;

    public PriceControllerAdapter(IGetPriceService getPriceService, PriceMapper priceMapper) {
        this.getPriceService = getPriceService;
        this.priceMapper = priceMapper;
    }

    @Override
    public Mono<PriceResponseDto> getPVP(int brandId, long product, LocalDateTime dateTime) {
        return getPriceService.getPVP(brandId, product, dateTime)
                .map(priceMapper::priceToResponse);
    }
}

