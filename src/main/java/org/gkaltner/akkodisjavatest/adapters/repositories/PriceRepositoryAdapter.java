package org.gkaltner.akkodisjavatest.adapters.repositories;

import lombok.extern.slf4j.Slf4j;
import org.gkaltner.akkodisjavatest.domain.model.Price;
import org.gkaltner.akkodisjavatest.domain.repositories.IPriceRepository;
import org.gkaltner.akkodisjavatest.infraestructure.mappers.PriceMapper;
import org.gkaltner.akkodisjavatest.infraestructure.repositories.IBrandH2Repository;
import org.gkaltner.akkodisjavatest.infraestructure.repositories.IPriceH2Repository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

@Slf4j
@Service
public class PriceRepositoryAdapter implements IPriceRepository {

    private final IPriceH2Repository priceH2Repository;
    private final IBrandH2Repository brandH2Repository;
    private final PriceMapper priceMapper;

    public PriceRepositoryAdapter(IPriceH2Repository priceH2Repository, IBrandH2Repository brandH2Repository, PriceMapper priceMapper) {
        this.priceH2Repository = priceH2Repository;
        this.brandH2Repository = brandH2Repository;
        this.priceMapper = priceMapper;
    }

    public Flux<Price> findByBrandIdProductAndDate(int brandId, long product, LocalDateTime dateTime) {
        return brandH2Repository.findById(brandId)
                .flatMapMany(brandEntity ->
                        priceH2Repository.findByBrandIdProductAndDate(brandId, product, dateTime)
                                .map(entity -> priceMapper.entityToPrice(entity, brandEntity)));
    }
}
