package org.gkaltner.akkodisjavatest.domain.model;

public record Brand(
        int id,
        String name
) {
}
