package org.gkaltner.akkodisjavatest.domain.repositories;

import org.gkaltner.akkodisjavatest.domain.model.Price;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

public interface IPriceRepository {
    Flux<Price> findByBrandIdProductAndDate(int brandId, long product, LocalDateTime dateTime);
}
