package org.gkaltner.akkodisjavatest.domain.services;

import org.gkaltner.akkodisjavatest.domain.exceptions.PriceNotFoundException;
import org.gkaltner.akkodisjavatest.domain.model.Price;
import org.gkaltner.akkodisjavatest.domain.repositories.IPriceRepository;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

public class GetPriceService implements IGetPriceService {
    private final IPriceRepository priceRepository;

    public GetPriceService(IPriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    @Override
    public Mono<Price> getPVP(int brandId, long product, LocalDateTime dateTime) {
        return this.priceRepository.findByBrandIdProductAndDate(brandId, product, dateTime)
                .reduce((price, price2) -> price.priority() > price2.priority() ? price : price2)
                .switchIfEmpty(Mono.error(new PriceNotFoundException("Price Not Found")));
    }
}
