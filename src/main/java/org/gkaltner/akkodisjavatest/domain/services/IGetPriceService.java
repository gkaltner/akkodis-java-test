package org.gkaltner.akkodisjavatest.domain.services;

import org.gkaltner.akkodisjavatest.domain.model.Price;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

public interface IGetPriceService {
    Mono<Price> getPVP(int brandId, long product, LocalDateTime dateTime);
}
