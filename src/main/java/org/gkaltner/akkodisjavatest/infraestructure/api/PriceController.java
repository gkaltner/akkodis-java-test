package org.gkaltner.akkodisjavatest.infraestructure.api;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.gkaltner.akkodisjavatest.adapters.controllers.IPriceControllerAdapter;
import org.gkaltner.akkodisjavatest.infraestructure.dtos.PriceRequestDto;
import org.gkaltner.akkodisjavatest.infraestructure.dtos.PriceResponseDto;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
public class PriceController implements IPriceController {
    private final IPriceControllerAdapter adapter;

    public PriceController(IPriceControllerAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public Mono<PriceResponseDto> getPVP(@Valid PriceRequestDto request) {
        return this.adapter.getPVP(request.getBrandId(), request.getProduct(), request.getDateTime());
    }
}
