package org.gkaltner.akkodisjavatest.infraestructure.configuration;

import org.gkaltner.akkodisjavatest.domain.repositories.IPriceRepository;
import org.gkaltner.akkodisjavatest.domain.services.GetPriceService;
import org.gkaltner.akkodisjavatest.domain.services.IGetPriceService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "org.gkaltner.akkodisjavatest.domain")
public class BeanConfiguration {
    @Bean
    public IGetPriceService getPriceService(final IPriceRepository priceRepository) {
        return new GetPriceService(priceRepository);
    }
}
