package org.gkaltner.akkodisjavatest.infraestructure.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PriceRequestDto {
    @NotNull
    @Schema(description = "Date to apply the price", example = "2020-06-14T00:00:00", format = "yyyy-MM-ddThh:mm:ss")
    private LocalDateTime dateTime;
    @Schema(description = "Product id to apply the price", example = "35455")
    @Min(value = 1, message = "product should be greater than 0")
    private long product;
    @Schema(description = "Brand ID", example = "1")
    @Min(value = 1, message = "Brand ID should be greater than 0")
    private int brandId;
}
