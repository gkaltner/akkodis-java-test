package org.gkaltner.akkodisjavatest.infraestructure.mappers;

import org.gkaltner.akkodisjavatest.domain.model.Price;
import org.gkaltner.akkodisjavatest.infraestructure.dtos.PriceResponseDto;
import org.gkaltner.akkodisjavatest.infraestructure.repositories.entities.BrandEntity;
import org.gkaltner.akkodisjavatest.infraestructure.repositories.entities.PriceEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {BrandEntity.class})
public interface PriceMapper {
    @Mapping(target = "brand", source = "brandEntity")
    @Mapping(target = "id", source = "entity.id")
    @Mapping(target = "product", source = "entity.product")
    @Mapping(target = "priority", source = "entity.priority")
    @Mapping(target = "priceList", source = "entity.priceList")
    @Mapping(target = "value", source = "entity.value")
    @Mapping(target = "currency", source = "entity.currency")
    @Mapping(target = "startDate", source = "entity.startDate")
    @Mapping(target = "endDate", source = "entity.endDate")
    Price entityToPrice(PriceEntity entity, BrandEntity brandEntity);

    @Mapping(target = "price", source = "value")
    @Mapping(target = "brand", source = "brand.name")
    PriceResponseDto priceToResponse(Price price);
}
