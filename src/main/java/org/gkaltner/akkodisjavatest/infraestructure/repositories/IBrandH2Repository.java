package org.gkaltner.akkodisjavatest.infraestructure.repositories;

import org.gkaltner.akkodisjavatest.infraestructure.repositories.entities.BrandEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBrandH2Repository extends ReactiveCrudRepository<BrandEntity, Integer> {
}
