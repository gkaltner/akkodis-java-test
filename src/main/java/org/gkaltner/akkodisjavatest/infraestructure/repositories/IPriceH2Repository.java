package org.gkaltner.akkodisjavatest.infraestructure.repositories;

import org.gkaltner.akkodisjavatest.infraestructure.repositories.entities.PriceEntity;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.UUID;

@Repository
public interface IPriceH2Repository extends ReactiveCrudRepository<PriceEntity, UUID> {

    @Query("""
             select *
             from PRICES p
             where p.BRAND_ID = :brandId
                 and p.PRODUCT = :product
                 and :dateTime between p.START_DATE and p.END_DATE
            """)
    Flux<PriceEntity> findByBrandIdProductAndDate(
            @Param("brandId") long brandId,
            @Param("product") long product,
            @Param("dateTime") LocalDateTime dateTime);
}
