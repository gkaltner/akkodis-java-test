package org.gkaltner.akkodisjavatest.infraestructure.repositories.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Table(name = "BRAND")
public class BrandEntity {
    @Id
    @Column("ID")
    private int id;
    @Column("NAME")
    private String name;
}
