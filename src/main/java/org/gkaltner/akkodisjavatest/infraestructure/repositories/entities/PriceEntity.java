package org.gkaltner.akkodisjavatest.infraestructure.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "PRICES")
public class PriceEntity {
    @Id
    @Column("ID")
    private UUID id;
    @Column("BRAND_ID")
    private long brandId;
    @Column("PRODUCT")
    private long product;
    @Column("PRIORITY")
    private int priority;
    @Column("PRICE_LIST")
    private int priceList;
    @Column("PRICE")
    private double value;
    @Column("CURRENCY")
    private String currency;
    @Column("START_DATE")
    private LocalDateTime startDate;
    @Column("END_DATE")
    private LocalDateTime endDate;
}
