package org.gkaltner.akkodisjavatest.infraestructure.api;

import org.gkaltner.akkodisjavatest.domain.services.IGetPriceService;
import org.gkaltner.akkodisjavatest.infraestructure.dtos.ErrorResponseDto;
import org.gkaltner.akkodisjavatest.infraestructure.dtos.PriceResponseDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebFlux;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@AutoConfigureWebFlux
@AutoConfigureWebTestClient
class PriceControllerTest {

    @Autowired
    private WebTestClient webClient;

    @SpyBean
    private IGetPriceService getPriceServiceSpy;

    public static Stream<Arguments> priceFilter() {
        return Stream.of(
                Arguments.of(35455, 1, LocalDateTime.of(2020, 6, 14, 10, 0), 1, 35.5),
                Arguments.of(35455, 1, LocalDateTime.of(2020, 6, 14, 16, 0), 2, 25.45),
                Arguments.of(35455, 1, LocalDateTime.of(2020, 6, 14, 21, 0), 1, 35.5),
                Arguments.of(35455, 1, LocalDateTime.of(2020, 6, 15, 10, 0), 3, 30.50),
                Arguments.of(35455, 1, LocalDateTime.of(2020, 6, 16, 21, 0), 4, 38.95)
        );
    }

    @ParameterizedTest
    @MethodSource("priceFilter")
    void shouldGetPVPTest(long product, int brandId, LocalDateTime dateTime, int priceList, double price) {
        final var priceResponse = webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/prices")
                        .queryParam("product", product)
                        .queryParam("brandId", brandId)
                        .queryParam("dateTime", dateTime)
                        .build())
                .exchange()
                .expectStatus().isOk()
                .expectBody(PriceResponseDto.class)
                .returnResult().getResponseBody();

        assertNotNull(priceResponse);
        assertEquals(price, priceResponse.getPrice());
        assertEquals(priceList, priceResponse.getPriceList());
        assertEquals("Zara", priceResponse.getBrand());
    }

    @Test
    void shouldResponseErrorIfIsInvalid() {
        int brandId = 0;
        long product = 0;
        LocalDateTime dateTime = LocalDateTime.now();

        var error = webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/prices")
                        .queryParam("product", product)
                        .queryParam("brandId", brandId)
                        .queryParam("dateTime", dateTime)
                        .build())
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ErrorResponseDto.class)
                .returnResult().getResponseBody();

        Assertions.assertNotNull(error);
        Assertions.assertNotNull(HttpStatus.BAD_REQUEST, error.getCode());
    }

    @Test
    void shouldResponseNotFoundIfNotExist() {
        int brandId = 1;
        long product = 35455;
        LocalDateTime dateTime = LocalDateTime.now();

        var error = webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/prices")
                        .queryParam("product", product)
                        .queryParam("brandId", brandId)
                        .queryParam("dateTime", dateTime)
                        .build())
                .exchange()
                .expectStatus().isNotFound()
                .expectBody(ErrorResponseDto.class)
                .returnResult().getResponseBody();

        Assertions.assertNotNull(error);
        Assertions.assertNotNull(HttpStatus.NOT_FOUND, error.getCode());
    }

    @Test
    void shouldResponseInternalError() {
        int brandId = 1;
        long product = 35455;
        LocalDateTime dateTime = LocalDateTime.now();

        Mockito.when(getPriceServiceSpy.getPVP(brandId, product, dateTime))
                .thenReturn(Mono.error(new RuntimeException("Internal err{or")));

        var error = webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/prices")
                        .queryParam("product", product)
                        .queryParam("brandId", brandId)
                        .queryParam("dateTime", dateTime)
                        .build())
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody(ErrorResponseDto.class)
                .returnResult().getResponseBody();

        Assertions.assertNotNull(error);
        Assertions.assertNotNull(HttpStatus.INTERNAL_SERVER_ERROR, error.getCode());
    }

}